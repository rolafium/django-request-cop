
test_settings = {
    "DEBUG": True,
    "DATABASES": {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
        },
    },
    "INSTALLED_APPS": (
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.admin",
        "django_request_cop",
    ),
    "ROOT_URLCONF": "django_request_cop.urls",
    "TEMPLATES": [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]


}

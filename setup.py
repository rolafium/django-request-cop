import os
from setuptools import find_packages, setup
from django_request_cop.version import VERSION


with open(os.path.join(os.path.dirname(__file__), "README.md")) as fp:
    README = fp.read()


setup(
    name="django_request_cop",
    version=VERSION,
    packages=find_packages(),
    include_package_data=True,
    license="Apache-2.0",
    description="Django Request Cop - Stop request spam",
    long_description=README,
    url="https://gitlab.com/rolafium/django-request-cop",
    author="William Di Pasquale",
    author_email="rolafium@protonmail.com",
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.0',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache-2.0',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        "django",
    ]
)

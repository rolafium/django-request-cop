import django, sys
from django.conf import settings
from django.test.runner import DiscoverRunner
from test_settings import test_settings


settings.configure(**test_settings)
django.setup()
test_runner = DiscoverRunner(verbosity=1)

failures = test_runner.run_tests(["django_request_cop"])
if failures:
    sys.exit(failures)

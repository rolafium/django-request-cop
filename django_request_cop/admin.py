from django.contrib import admin
from django_request_cop.models import (
    IPRequestBlock, 
    GoogleRecaptchaRequestBlock,
)

admin.site.register(IPRequestBlock)
admin.site.register(GoogleRecaptchaRequestBlock)

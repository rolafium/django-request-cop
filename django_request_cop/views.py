from django.conf import settings
from django.shortcuts import render
from django_request_cop.models import GoogleRecaptchaRequestBlock


def google_recaptcha_landing(request):
    """Loads the landing template for the Google Recaptcha Test"""
    template = "django_request_cop/google_recaptcha_landing.html";
    request_cop_base_template = getattr(
        settings,
        "REQUEST_COP_BASE_TEMPLATE", 
        "django_request_cop/default_base_template.html",
    )
    
    keys = GoogleRecaptchaRequestBlock.get_recaptcha_keys()

    context = {
        "request_cop_base_template": request_cop_base_template,
        "recaptcha_public_key": keys["recaptcha_public_key"],
        "recaptcha_private_key": keys["recaptcha_private_key"],
    }

    return render(request, template, context)

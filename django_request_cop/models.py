import json
import urllib.request, urllib.parse
from datetime import timedelta
from hashlib import sha1
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db import models
from django.db.models import Q
from django.utils import timezone


class RequestBlock(models.Model):
    """An abstract parent for all the request block trackers"""

    class Meta:
        abstract = True

    MAX_REQUESTS = 5

    DEFAULT_EXPIRE_DELTA = { "days": 1 }

    EXPIRE_TIME_DELTA = timedelta(
        **getattr(
            settings, 
            "REQUEST_COP_EXPIRE_DELTA",
            DEFAULT_EXPIRE_DELTA
        ) 
    )

    request_area = models.CharField(max_length=64)
    request_method = models.CharField(max_length=16)
    request_path = models.TextField()
    expires = models.DateTimeField()

    @classmethod
    def log_request(cls, request, request_area):
        """
        Logs a request
        :param request: WSGI Request
        :param request_area: str
        :returns: not saved RequestBlock (we can't save as it's abstract)
        """
        expires = timezone.now() + cls.EXPIRE_TIME_DELTA
        return cls(
            request_area=request_area,
            request_method=request.method,
            request_path=request.build_absolute_uri(),
            expires=expires,
        )

    @classmethod
    def _get_ip_from_request(cls, request):
        """
        Finds out the IP of the request
        :param request: WSGI Request
        :returns: str
        """
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')

        return ip

    @classmethod
    def clean_expired_entries(cls):
        """Removes all the expired entries from the database"""
        now = timezone.now()
        cls.objects.filter(expires__lte=now).delete()

    @classmethod
    def validate(cls, request, request_area, *args, **kwargs):
        """
        Logs and validates a request. Raises PermissionDenied if we reached too many requests
        :param request: WSGI Request
        :param request_area: str
        :raises: django.core.exceptions.PermissionDenied
        """
        instance = cls.log_request(request, request_area, *args, **kwargs)
        instance.save()
        now = timezone.now()
        requests_count = cls.objects.filter(instance.get_query(request)).filter(expires__gt=now).count()

        skip_validation = getattr(settings, "REQUEST_COP_SKIP_VALIDATION", False)
        if not skip_validation and requests_count > cls.MAX_REQUESTS:
            cls.block_request(request)

    @classmethod
    def block_request(self, request):
        """
        Raises a permission denied
        :param request: WSGI Request
        :raises: django.core.exceptions.PermissionDenied
        """
        raise PermissionDenied()

    def get_query(self, request):
        """
        Returns the Q object that filters the previous requests
        :param request: WSGI Request
        :returns: django.db.models.Q
        """
        return Q()

    def __str__(self):
        """String default representation of the object"""
        return "{cls} - {area} - {expires}".format(
            cls=self.__class__.__name__,
            area=self.request_area,
            expires=self.expires,
        )


class IPRequestBlock(RequestBlock):

    MAX_REQUESTS = getattr(settings, "REQUEST_COP_IP_REQUEST_MAX", 5)

    ip_hash = models.CharField(max_length=64)

    @classmethod
    def log_request(cls, request, request_area):
        """
        Logs and saves the request
        :param request: WSGI Request
        :param request_area: str
        :returns: Instance of IPRequestBlock
        """
        instance = super().log_request(request, request_area)
        instance.ip_hash = cls.get_ip_hash(cls._get_ip_from_request(request))
        return instance

    @classmethod
    def get_ip_hash(cls, ip):
        """
        Produces the sha1 hash of the ip
        :param ip: str
        :returns: str
        """
        return sha1(ip.encode()).hexdigest()

    def get_query(self, request):
        """
        Returns the Q object that filters the previous requests
        :param request: WSGI Request
        :returns: django.db.models.Q
        """        
        return Q(ip_hash=self.ip_hash)



class GoogleRecaptchaRequestBlock(IPRequestBlock):

    MAX_REQUESTS = getattr(settings, "REQUEST_COP_GOOGLE_RECAPTCHA_MAX", 5)

    recaptcha_failure = models.BooleanField()

    @classmethod
    def get_google_confirmation(cls, request, google_response):
        """
        Sends a request of confirmation to Google Recaptcha
        :param request: WSGI Request
        :param google_response: str passed by the frontend
        :returns: bool
        """
        google_url = "https://www.google.com/recaptcha/api/siteverify"
        method = "POST"
        keys = cls.get_recaptcha_keys()

        data = {
            "secret": keys["recaptcha_private_key"],
            "response": google_response,
        }

        data = bytes(urllib.parse.urlencode(data).encode())
        response = urllib.request.urlopen(google_url, data);
        content = json.loads(response.read())

        return content["success"]

    @classmethod
    def get_recaptcha_keys(cls):
        """
        Returns the keys provided by Google Recaptcha
        :raises: AttributeError when the keys are not set in the settings
        :returns: dict
        """
        recaptcha_public_key_name = "REQUEST_COP_RECAPTCHA_PUBLIC_KEY"
        recaptcha_private_key_name = "REQUEST_COP_RECAPTCHA_PRIVATE_KEY"

        try:
            return {
                "recaptcha_public_key": getattr(settings, recaptcha_public_key_name),
                "recaptcha_private_key": getattr(settings, recaptcha_private_key_name),
            }
        except AttributeError:
            exception_message = "{cls} requires settings {public_key} and {private_key}".format(
                cls=cls.__name__,
                public_key=recaptcha_public_key_name,
                private_key=recaptcha_private_key_name,
            )
            raise AttributeError(exception_message)

    @classmethod
    def log_request(cls, request, request_area, google_response=None):
        """
        Logs and saves the request, checking with google if the recaptcha was valid
        :param request: WSGI Request
        :param request_area: str
        :raises: PermissionDenied if recaptcha fails
        :returns: Instance of GoogleRecaptchaRequestBlock
        """
        if google_response is None:
            raise AttributeError("Google Recaptcha Response is required.")

        instance = super().log_request(request, request_area)
        instance.recaptcha_failure = not cls.get_google_confirmation(request, google_response)

        if instance.recaptcha_failure:
            cls.block_request(request)
            
        return instance

    def get_query(self, request):
        """
        Returns the Q object that filters the previous requests
        :param request: WSGI Request
        :returns: django.db.models.Q
        """        
        return super().get_query(request) & Q(recaptcha_failure=True)

from django.urls import path
from django_request_cop.views import (
    google_recaptcha_landing, 
)


urlpatterns = [
    # Google Recaptcha Test Landing
    path(
        "django-request-cop/google-recaptcha",
        google_recaptcha_landing,
        name="request_cop_google_recaptcha_landing",
    ),
]

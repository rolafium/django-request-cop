
const djangoRequestCopChannel = new BroadcastChannel("DJANGO_REQUEST_COP_CHANNEL");


const djangoRequestCopRecaptchaSuccessInternal = (response) => {
    const message = {
        name: "REQUEST_COP_RECAPTCHA_RESPONSE",
        googleResponse: response,
    };

    djangoRequestCopChannel.postMessage(message);
}

const djangoRequestCopRecaptchaLoad = () => {
    grecaptcha.render("django-request-cop-recaptcha-container", {
        sitekey: REQUEST_COP_RECATCHA_PUBLIC_KEY,
        callback: djangoRequestCopRecaptchaSuccessInternal,
    });
}

window.djangoRequestCopRecaptchaSuccessInternal = djangoRequestCopRecaptchaSuccessInternal;
window.djangoRequestCopRecaptchaLoad = djangoRequestCopRecaptchaLoad;

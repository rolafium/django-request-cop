from django.apps import AppConfig


class DjangoRequestCopConfig(AppConfig): # pragma: no cover
    name = 'django_request_cop'

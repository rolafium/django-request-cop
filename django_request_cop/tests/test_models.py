from datetime import timedelta
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.test import TestCase, override_settings
from django.test.client import RequestFactory
from django.utils import timezone
from django_request_cop.tests import (
    override_settings_and_recaptcha,
    GOOGLE_RECAPTCHA_TEST_PUBLIC_KEY,
    GOOGLE_RECAPTCHA_TEST_PRIVATE_KEY,
)
from django_request_cop.models import (
    RequestBlock,
    IPRequestBlock,
    GoogleRecaptchaRequestBlock,
)


class TestModelsMixin(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.rf = RequestFactory()
        cls.ip = "127.0.0.1"
        cls.default_request = cls.rf.get("/")


    @classmethod
    def tearDownClass(cls):
        pass


class TestRequestBlock(TestModelsMixin):

    def test_attributes(self):
        """Tests it has all the required attributes"""
        self.assertTrue(RequestBlock.MAX_REQUESTS > 0)
        self.assertTrue(isinstance(RequestBlock.EXPIRE_TIME_DELTA, timedelta))
        self.assertTrue(isinstance(RequestBlock().__str__(), str))
        self.assertTrue(isinstance(RequestBlock().get_query(self.default_request), Q))

    def test_log_request(self):
        """Tests it creates the instance"""
        before = timezone.now() + RequestBlock.EXPIRE_TIME_DELTA
        request = self.rf.get("/")
        instance = RequestBlock.log_request(request, "TEST_AREA")
        after = timezone.now() + RequestBlock.EXPIRE_TIME_DELTA

        self.assertEqual(instance.request_area, "TEST_AREA")
        self.assertEqual(instance.request_method, request.method)
        self.assertEqual(instance.request_path, request.build_absolute_uri())
        self.assertTrue(instance.expires > before)
        self.assertTrue(instance.expires < after)


    def test_get_ip_from_request(self):
        """Tests it gets the IP from the request"""
        request_normal = self.rf.get("/")
        request_normal.META["REMOTE_ADDR"] = self.ip

        request_forward = self.rf.get("/")
        request_forward.META["HTTP_X_FORWARDED_FOR"] = self.ip

        ip_normal = RequestBlock._get_ip_from_request(request_normal)
        ip_forward = RequestBlock._get_ip_from_request(request_forward)

        self.assertEqual(self.ip, ip_normal)
        self.assertEqual(self.ip, ip_forward)


class TestIPRequestBlock(TestModelsMixin):

    def test_attributes(self):
        """Tests it has all the required attributes"""
        self.assertTrue(isinstance(IPRequestBlock.MAX_REQUESTS, int))
        self.assertTrue(isinstance(IPRequestBlock().get_query(self.default_request), Q))

    def test_clean_expired_entries(self):
        """Tests it deletes the expired entries"""
        one_day = timedelta(days=1)
        before = timezone.now() - one_day
        after = timezone.now() + one_day

        before_entry = IPRequestBlock.objects.create(
            ip_hash="aaaa",
            request_area="TEST_AREA",
            request_method="GET",
            request_path="/",
            expires=before,
        )

        after_entry = IPRequestBlock.objects.create(
            ip_hash="aaaa",
            request_area="TEST_AREA",
            request_method="GET",
            request_path="/",
            expires=after,
        )

        first_queryset = list(IPRequestBlock.objects.all())
        self.assertTrue(before_entry in first_queryset)
        self.assertTrue(after_entry in first_queryset)

        IPRequestBlock.clean_expired_entries()
        second_queryset = list(IPRequestBlock.objects.all())
        self.assertFalse(before_entry in second_queryset)
        self.assertTrue(after_entry in second_queryset)

    def test_get_ip_hash(self):
        """Tests it creates an IP hash"""
        _hash1 = IPRequestBlock.get_ip_hash(self.ip)
        _hash2 = IPRequestBlock.get_ip_hash(self.ip)
        self.assertTrue(isinstance(_hash1, str))
        self.assertTrue(isinstance(_hash2, str))
        self.assertTrue(self.ip != _hash1)
        self.assertTrue(_hash1 == _hash2)


    def test_log_request(self):
        """Tests it creates the instance"""
        instance = IPRequestBlock.log_request(self.default_request, "TEST_AREA")

        self.assertTrue(isinstance(instance.ip_hash, str))
        self.assertTrue(instance.ip_hash != "")
        self.assertEqual(instance.request_area, "TEST_AREA")


    def test_validate(self):
        """Tests it validates the request"""
        IPRequestBlock.objects.all().delete()

        for _ in range(IPRequestBlock.MAX_REQUESTS):
            IPRequestBlock.validate(self.default_request, "TEST_AREA")

        self.assertRaises(PermissionDenied, IPRequestBlock.validate, self.default_request, "TEST_AREA")


class TestGoogleRecaptchaRequestBlock(TestModelsMixin):

    def test_attributes(self):
        """Tests it has all the required attributes"""
        self.assertTrue(isinstance(GoogleRecaptchaRequestBlock.MAX_REQUESTS, int))
        self.assertTrue(isinstance(GoogleRecaptchaRequestBlock().get_query(self.default_request), Q))

    def test_get_recaptcha_keys_fail(self):
        """Tests it fails to retrieve the recaptcha keys"""
        self.assertRaises(AttributeError, GoogleRecaptchaRequestBlock.get_recaptcha_keys)

    @override_settings_and_recaptcha()
    def test_get_recaptcha_keys_success(self):
        """Tests it successfully retrieves the recaptcha keys"""
        keys = GoogleRecaptchaRequestBlock.get_recaptcha_keys()
        self.assertEqual(keys["recaptcha_public_key"], GOOGLE_RECAPTCHA_TEST_PUBLIC_KEY)
        self.assertEqual(keys["recaptcha_private_key"], GOOGLE_RECAPTCHA_TEST_PRIVATE_KEY)

    @override_settings_and_recaptcha()
    def test_get_google_confirmation(self):
        """Tests it correctly receives an answer from google recaptcha"""
        response = GoogleRecaptchaRequestBlock.get_google_confirmation(self.default_request, "GOOGLE_RESPONSE")
        self.assertTrue(response)

    @override_settings_and_recaptcha()
    def test_log_request(self):
        """Tests it creates the instance"""
        self.assertRaises(
            AttributeError,
            GoogleRecaptchaRequestBlock.log_request,
            self.default_request,
            "TEST_AREA",
        )

        instance = GoogleRecaptchaRequestBlock.log_request(self.default_request, "TEST_AREA", google_response="GOOGLE_RESPONSE")

        self.assertTrue(isinstance(instance.ip_hash, str))
        self.assertTrue(isinstance(instance.recaptcha_failure, bool))
        self.assertFalse(instance.recaptcha_failure)
        self.assertEqual(instance.request_area, "TEST_AREA")

    @override_settings(
        REQUEST_COP_RECAPTCHA_PUBLIC_KEY="TEST_PUBLIC_KEY",
        REQUEST_COP_RECAPTCHA_PRIVATE_KEY="TEST_PRIVATE_KEY",
    )
    def test_log_request_failure(self):
        self.assertRaises(
            PermissionDenied,
            GoogleRecaptchaRequestBlock.log_request,
            self.default_request,
            "TEST_AREA",
            google_response="GOOGLE_RESPONSE",
        )

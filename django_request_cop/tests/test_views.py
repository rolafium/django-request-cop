from django.urls import reverse
from django.test import TestCase, Client, override_settings
from django_request_cop.tests import override_settings_and_recaptcha
from django_request_cop.views import google_recaptcha_landing


class TestViews(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.client = Client()

    @classmethod
    def tearDownClass(cls):
        pass

    def test_google_recaptcha_landing_fails(self):
        """Tests it fails to load the recaptcha landing page"""
        url = reverse(google_recaptcha_landing)
        self.assertRaises(AttributeError, self.client.get, url)


    @override_settings_and_recaptcha()
    def test_google_recaptcha_landing(self):
        """Tests it correctly retrieves the recaptcha landing page"""
        url = reverse(google_recaptcha_landing)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue("request_cop_base_template" in response.context)
        self.assertTrue("recaptcha_public_key" in response.context)
        self.assertTrue("recaptcha_private_key" in response.context)

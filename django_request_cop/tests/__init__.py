from django_request_cop.models import GoogleRecaptchaRequestBlock
from django.test import override_settings


GOOGLE_RECAPTCHA_TEST_PUBLIC_KEY = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
GOOGLE_RECAPTCHA_TEST_PRIVATE_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"


def override_settings_and_recaptcha(*args, **kwargs):
    """
    Overrides the settings with **kwargs and the recaptcha keys
    Util to easily decorate tests that use the GoogleRecaptchaRequestBlock
    :param **kwargs:
    :returns: function
    """
    return override_settings(
        *args,
        REQUEST_COP_RECAPTCHA_PUBLIC_KEY=GOOGLE_RECAPTCHA_TEST_PUBLIC_KEY,
        REQUEST_COP_RECAPTCHA_PRIVATE_KEY=GOOGLE_RECAPTCHA_TEST_PRIVATE_KEY,
        **kwargs,
    )

# Django Request Cop
![pipeline status](https://gitlab.com/rolafium/django-request-cop/badges/master/pipeline.svg)
![coverage](https://gitlab.com/rolafium/django-request-cop/badges/master/coverage.svg)

Django Request Cop is a Django library that allows you to easily block requests on different criteria:
* IP Block - Block requests on a specific resource based on the client IP
* Google Recaptcha - Block requests on a specific resource based on Google Recaptcha hard failures (i.e. sending to the server false data)


# Requirements
Tested on Python 3.6 and Django 2.0 - No dependencies


# Installation
* pip install from git: `pip install git+https://gitlab.com/rolafium/django-request-cop.git`
* Add django_request_cop.urls to your url config
* Add `django_request_cop` specific settings to your settings file (See `Available Settings` section)
* Run the migrations


# Usage
On your view (or anywhere you have access to a WSGI Request object to pass down...), import the request block class you need and call the `validate` method with the required arguments.

### django_request_cop.models.IPRequestBlock

Execute `IPRequestBlock.validate(request, request_area)`, where:
* `request` is your WSGI Request object
* `request_area` is an arbitrary string tag (i.e. `USER_REGISTRATION`)

### django_request_cop.models.GoogleRecaptcha

Include `django_request_cop/google_recaptcha_iframe.html` on your page. This will expose a couple of window function that you can use to trigger the Google Recaptcha check:
* `window.drcOpenIframe` - Arguments: `onSuccess`, `onError`. This will allow to expose the widget only on actual application calls to recaptcha (so it can be used in Single Page Applications). The `onSuccess` and `onError` arguments are callbacks called respectively on success with the data response provided by Google or on error(i.e. the user closes the widget). The function can be called multiple times to define different entry points for Google Recaptcha. It will also clean up itself (removing the Broadcast Channer and the iframe).
* `window.drcOpenIframePromise` - Same as `window.drcOpenIframe` but returns a `Promise` instead that resolves on success and rejects on error.

Send to the server the response provided by google and call in your view:
`GoogleRecaptchaRequestBlock.validate(request, request_area, google_response="your_frontend_google_response")`, where:
* `request` is your WSGI Request object
* `request_area` is an arbitrary string tag (i.e. `USER_REGISTRATION`)
* `google_response` is a keyword argument containg the Google Recaptcha response string sent from the frontend.
The class will log the request and trigger PermissionDenied if required.


# Available Settings
* REQUEST_COP_EXPIRE_DELTA - How long until the request expires (`dict` to be consumed by `datetime.timedelta`, default: `{ days: 1}`)
* REQUEST_COP_SKIP_VALIDATION - Logs the request in `validate` but will skip raising `PermissionDenied`. Use it in tests (`boolean`)
* REQUEST_COP_IP_REQUEST_MAX - Max `IPRequestBlock` unexpired requests before triggering `PermissionDenied` (integer, default: `5`)
* REQUEST_COP_GOOGLE_RECAPTCHA_MAX - Max `GoogleRecaptchaRequestBlock` unexpired requests before triggering `PermissionDenied` regardless of the results of the recaptcha (Please note that PermissionDenied is raised at each fail regardless of this number) (integer, default: `5`)
* REQUEST_COP_RECAPTCHA_PUBLIC_KEY - Required when using `GoogleRecaptchaRequestBlock`. Key provided by Google. AttributeError is raised by the class when the setting is not defined
* REQUEST_COP_RECAPTCHA_PRIVATE_KEY - Required when using `GoogleRecaptchaRequestBlock`. Key provided by Google. AttributeError is raised by the class when the setting is not defined


# Test Utils
* The setting `REQUEST_COP_SKIP_VALIDATION` skips the validation. Use it when you are not interested by the validation and want to allow any number of requests to the protected resource (i.e. testing the resource with multiple data)
* When using the `GoogleRecaptchaRequestBlock` on your views, you may want to use Google's test keys. There is a commodity decorator imported with `from django_request_cop.tests import override_settings_and_recaptcha`. This decorator works similarly to django's `override_settings`, accepts a list **kwargs that will be the overriden settings and adds the google keys in `REQUEST_COP_RECAPTCHA_PUBLIC_KEY` and `REQUEST_COP_RECAPTCHA_PRIVATE_KEY`. Please note that if you only want to pass the keys, not overriding any other settings, you will need to decorate your test as `@override_settings_and_recaptcha()`. Please check `django_request_cop/tests/test_models.py` for an example.
